package com.example.vijayreddy.myapplication;

public class QuestsItem {
    private String questName;
    private String postedBy;
    private String reward;

    public String getQuestName() {
        return questName;
    }

    public void setQuestName(String questName) {
        this.questName = questName;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    @Override
    public String toString() {
        return "[ Quest Name=" + questName + ", Posted By=" +
                postedBy + " , Reward=" + reward + "]";
    }
}
