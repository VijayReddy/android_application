package com.example.vijayreddy.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapter extends BaseAdapter {

    private ArrayList listData;

    private LayoutInflater layoutInflater;

    public CustomListAdapter(Context context, ArrayList listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout, null);
            holder = new ViewHolder();
            holder.questNameView = (TextView) convertView.findViewById(R.id.title);
            holder.postedByView = (TextView) convertView.findViewById(R.id.creator);
            holder.rewardView = (TextView) convertView.findViewById(R.id.reward);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        QuestsItem questsItem = (QuestsItem)listData.get(position);
        holder.questNameView.setText(questsItem.getQuestName());
        holder.postedByView.setText("Posted By:  " + questsItem.getPostedBy());
        holder.rewardView.setText(questsItem.getReward());

        return convertView;
    }

    static class ViewHolder {
        TextView questNameView;
        TextView postedByView;
        TextView rewardView;
    }

}
