package com.example.vijayreddy.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.ArrayList;

public class QuestMain extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quest_main);

        ArrayList image_details = getListData();
        final ListView lv1 = (ListView) findViewById(R.id.custom_list);
        lv1.setAdapter(new CustomListAdapter(this, image_details));
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                if(position==0)
                {
                    final Intent detailed_quests = new Intent(QuestMain.this, Quest1Details.class);
                    startActivity(detailed_quests);
                }
                else if(position==1)
                {
                    final Intent detailed_quests = new Intent(QuestMain.this, Quest2Details.class);
                    startActivity(detailed_quests);
                }
                else if(position==2)
                {
                    final Intent detailed_quests = new Intent(QuestMain.this, Quest3Details.class);
                    startActivity(detailed_quests);
                }

            }

        });

    }

    private ArrayList getListData() {
        ArrayList results = new ArrayList();
        QuestsItem questData = new QuestsItem();
        questData.setQuestName("Bandits in the Woods");
        questData.setPostedBy("HotDogg, The Bounty Hunter");
        questData.setReward("Reward: 10000 Gold, 10000 XP");
        results.add(questData);

        questData = new QuestsItem();
        questData.setQuestName("Special Delivery");
        questData.setPostedBy("Sir Jimmy, The Swift");
        questData.setReward("Reward: 10000 Gold, 10000 XP");
        results.add(questData);

        questData = new QuestsItem();
        questData.setQuestName("Filthy Mongrel");
        questData.setPostedBy("Prince Jack, The Iron Horse");
        questData.setReward("Reward: 10000 Gold, 10000 XP");
        results.add(questData);

        return results;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            final Intent settings = new Intent(QuestMain.this, Settings.class);
            startActivity(settings);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
