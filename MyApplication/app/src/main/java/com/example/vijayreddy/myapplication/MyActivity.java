package com.example.vijayreddy.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;

public class MyActivity extends Activity {

    private EditText nameEditText,passwordEditText;
    private Button submitButton;
    private String username;
    private CheckBox saveLoginCheckBox;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        submitButton=(Button)findViewById(R.id.btn_submit);
        nameEditText=(EditText)findViewById(R.id.editText);
        passwordEditText=(EditText)findViewById(R.id.editText1);
        saveLoginCheckBox = (CheckBox)findViewById(R.id.checkBox);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            nameEditText.setText(loginPreferences.getString("username", ""));
            saveLoginCheckBox.setChecked(true);
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String Name=nameEditText.getText().toString();
                final String Password=passwordEditText.getText().toString();

                if (view == submitButton) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(nameEditText.getWindowToken(), 0);

                    username = nameEditText.getText().toString();


                    if (saveLoginCheckBox.isChecked()) {
                        loginPrefsEditor.putBoolean("saveLogin", true);
                        loginPrefsEditor.putString("username", username);
                        loginPrefsEditor.commit();
                    } else {
                        loginPrefsEditor.clear();
                        loginPrefsEditor.commit();
                    }
                }

                if(Name.length()==0)
                {
                    nameEditText.requestFocus();
                    nameEditText.setError("REQUIRED");
                }
                if(Password.length()==0)
                {
                    passwordEditText.requestFocus();
                    passwordEditText.setError("REQUIRED");
                }
                if(Name.matches("Lancelot") && Password.matches("arthurDoesntKnow"))
                {
                    final Intent quests = new Intent(MyActivity.this, QuestMain.class);
                    startActivity(quests);

                }
                else if(Name.length()!=0 && Password.length()!=0)
                {
                    Toast.makeText(MyActivity.this,"Username and/or password is incorrect",Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
