package com.example.vijayreddy.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class Settings extends Activity implements OnItemSelectedListener {
    Spinner spinnerAlignment;
    private String[] alignment = { "Neutral", "Good", "Evil" };
    private Button updateButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_settings);
        updateButton=(Button)findViewById(R.id.btn_update);
       spinnerAlignment = (Spinner) findViewById(R.id.alignment);
        ArrayAdapter<String> adapter_alignment = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, alignment);
        adapter_alignment
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAlignment.setAdapter(adapter_alignment);
        spinnerAlignment.setOnItemSelectedListener(this);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        spinnerAlignment.setSelection(position);
        final String alignment_selected = (String) spinnerAlignment.getSelectedItem();
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(alignment_selected.equals("Good")) {
                    final Intent quest_good = new Intent(Settings.this, QuestGood.class);
                    startActivity(quest_good);
                }
                else if(alignment_selected.equals("Evil")) {
                    final Intent quest_evil = new Intent(Settings.this, QuestEvil.class);
                    startActivity(quest_evil);
                }
                else {
                    final Intent quest_neutral = new Intent(Settings.this, QuestMain.class);
                    startActivity(quest_neutral);
                }
            }
        });
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }


}
